using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MovementBehavior : MonoBehaviour
{
    //Movement
    public float speed;
    public GameObject radio;
    public Text dialogue;
    public Camera mainCamera;
    public float writingSpeed;
    public bool sparks = false;

    [SerializeField]
    private Rigidbody2D _body;
    [SerializeField]
    private int itemCount;
    [SerializeField]
    private int indexDialogue;
    private float moveVelocity;
    [SerializeField]
    private string [] currentDialogue = { "Teste", "teste" };
    private bool skipped = false;
    public bool canMove = true;
    private Coroutine typer;
    private Collider2D currentCollider; //Oh man, I have no idea what i'm doing anymore.
    private Animator animator;

    public AudioSource letterSound;
    public AudioSource radioSound;
    public GameObject baterias;
    public GameObject metais;
    public GameObject cabos;

    public GameObject warningSign;

    public bool ending = false;

    void Awake()
    {
        _body = gameObject.GetComponent<Rigidbody2D>();
        mainCamera = Camera.main;
        dialogue = GameObject.Find("Dialogue").GetComponent<Text>();
        dialogue.text = "";
        animator = gameObject.GetComponent<Animator>();
    }

    private void Start()
    {
        typer = StartCoroutine(ITypeWriter(""));
        warningSign.SetActive(false);
        canMove = false;
        indexDialogue = 0;
        currentDialogue = "...; ...; Oh, God.; I must have been hit by some kind of projectile.; My memory circuit is damaged, though not entirely gone.; Goddamit!; My head hurts.; But maybe it's because there's no other place to hurt anymore.; ...; I wish I was home.; Anyway, I need to ask for help.;But.; My radio parts are broken.; Perhaps I can build one from space trash. Just wish I had my legs intact.; At least I could kick somebody with them.".Split(';');
        StartCoroutine(IFadeAndDestroy());

    }

    void Update()
    {
        moveVelocity = 0;

        if (Input.GetKeyDown(KeyCode.Escape ))
        {
            Application.Quit();
        }

        if (canMove)
        {
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {

                moveVelocity = -speed;

            }

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                moveVelocity = speed;

            }

            warningSign.SetActive(false);

        } else
        {
            if(Input.GetKeyDown(KeyCode.Z))
            {

                if (skipped)
                {
                    StartTyper();
                }
                else
                {
                    if(typer != null)
                    {
                        StopCoroutine(typer);
                    }
                    skipped = true;
                    dialogue.text = currentDialogue[indexDialogue];
                    indexDialogue++;
                }

            }
        }

        if(skipped && !canMove)
        {
            warningSign.SetActive(true);
        }
        else
        {
            warningSign.SetActive(false);
        }

        if (!ending)
        {
            if (itemCount >= 3 && canMove)
            {
                transform.position = new Vector2(radio.transform.position.x - 5f, transform.position.y);
                radio.GetComponent<Renderer>().enabled = true;
                currentDialogue = ("Okay.; Now that I have all the parts that I needed.; I can now call home.; ...; ...;" +
                    " It sure is taking a while.; Maybe they forgot about me or something.; *bzzzzt. llo!? Can you hear me? *bzzt;" +
                    "Hey! I know I've got no ears, but yeah, I can hear you!; If you are listening to this *bzzzzt. it's already too late...;" +
                    "My dude, I didn't even finish my sentence!; I must say that the planet earth, our home, has reached its limits; ; ;" +
                    " *bzzzt Details shall be spared. But we are soon to disapear.; And all because *bzzzzzzzt we couldn't agree with each other. ; How stupid is this?; It's almost comical!; " +
                    "It always reminds me of the tale of the *bzzzt Tower of Babel.;...;" +
                    "I always saw this tale as God not taking away one language *bzzzzt or one speech.; But our capacity to truly understand each other.;" +
                    "And it saddens me that the lack of empathy *bzzzzt has led us to an end.; So, if you are listening to this; Don't forget that violence is never the answer;" +
                    "We possessed too great of a power to lose control over petty things.; So I hope that others can learn from our mistakes.; *click;" +
                    "; ;Oh, no!; I can not believe what they've done!; ...; I'm bound to eternity in this place.; I rather erase my own memory, even if only a tiny part.;" +
                    "So I can at least have hope to return.; Even if it means to collect parts forever, in a meaningless task.; Like a robotic Sisyphus; I shall do that till none is left.; And when that happens, I will not listen to this transmission.; " +
                    "But also will never know the thruth; ...; " +
                    "*Erasing memorier*; I wish none of this had happened.; *Breaks radio ; *Memory erased*; " ).Split(';');
                StartCoroutine(IFadeAndDestroy());
                canMove = false;
                ending = true;
            }
        }
        else
        {
            if (indexDialogue == 7)
            {
                radioSound.Play();
            }

            if (indexDialogue == 25)
            {
                radioSound.Stop();
            }

            if ( indexDialogue == 13)
            {
                sparks = true;
            }

            if (indexDialogue >= currentDialogue.Length)
            {
                StartCoroutine(IEndScene());
            }

            if (indexDialogue >= currentDialogue.Length -1)
            {
                radio.GetComponent<Renderer>().enabled = false;
            }
        }

        animator.SetBool("Sparks", sparks);
        GetComponent<Rigidbody2D>().velocity = new Vector2(moveVelocity, GetComponent<Rigidbody2D>().velocity.y);
    }

    void StartTyper ()
    {
        if (indexDialogue < currentDialogue.Length)
        {
            typer = StartCoroutine(ITypeWriter(currentDialogue[indexDialogue]));
        }
        else
        {
            StartCoroutine(IEndCutScene(currentCollider));
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        currentCollider = collider;

        if(canMove)
        {
            if (collider.tag == "Parts")
            {
                itemCount++;
                canMove = false;
                StartCoroutine(IFadeAndDestroy(collider));
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Wall")
        {
            StopCoroutine(typer);
            currentDialogue = "I don't need to go too far away. There's plenty of trash in this nobody's land.".Split(';');
            typer = StartCoroutine(ITypeWriter(currentDialogue[indexDialogue]));
            indexDialogue = 0;
            Invoke("CleanDialogue", 9.5f);
        }
    }

    void NextLine ()
    {
        StopCoroutine(typer);
        indexDialogue++;
        skipped = true;
        typer = StartCoroutine(ITypeWriter(currentDialogue[indexDialogue]));
    }

    void CleanDialogue ()
    {
        StopCoroutine(typer);
        dialogue.text = "";
    }

    IEnumerator IFadeAndDestroy (Collider2D collider)
    {
        CameraBehavior behavior = mainCamera.GetComponent<CameraBehavior>();
        StopCoroutine(typer);
        behavior.fadeDirection = 1;
        yield return new WaitForSeconds(1f);
        transform.rotation = new Quaternion(0, 0, 0, 0);
        behavior.fadeDirection = -1;
        yield return new WaitForSeconds(.8f);

        if (collider.name == "Satelite")
        {
            currentDialogue = "I recognize this.; This one is from a well known company.; It shoots soda cans to people from space.; Unfortunetly it hab very bad aim, and hurt some innocents.; Some actual customers too.; I should try to pull the cables from it.; ".Split(';');
            typer = StartCoroutine(ITypeWriter(currentDialogue[indexDialogue]));
        }
        else if (collider.name == "Foguete")
        {
            currentDialogue = "The first french fries space missile.; It never really exploded. In many senses; ".Split(';');
            typer = StartCoroutine(ITypeWriter(currentDialogue[indexDialogue]));
        }
        else if (collider.name == "Celular-gigante")
        {
            currentDialogue = "People wanted big screens so bad; that they never stopped to think if they should do it.; By the way, I can get one of the hundred batteriese inside.; ".Split(';');
            typer = StartCoroutine(ITypeWriter(currentDialogue[indexDialogue]));
        }
    }

    IEnumerator IFadeAndDestroy()
    {
        CameraBehavior behavior = mainCamera.GetComponent<CameraBehavior>();
        StopCoroutine(typer);
        behavior.fadeDirection = 1;
        yield return new WaitForSeconds(1f);
        warningSign.SetActive(true);
        indexDialogue = 0;
        transform.rotation = new Quaternion(0, 0, 0, 0);
        behavior.fadeDirection = -1;
        yield return new WaitForSeconds(.8f);

        typer = StartCoroutine(ITypeWriter(currentDialogue[indexDialogue]));
    }

    IEnumerator ITypeWriter (string text)
    {
        skipped = false;
        for (int i = 0; i <= text.Length-1; i++)
        {
            dialogue.text = text.Substring(0, i);
            letterSound.Play();
            if (text[i] == ',' || text[i] == '.')
            {
                yield return new WaitForSeconds(writingSpeed + 0.4f);
            }
            else
            {
                yield return new WaitForSeconds(writingSpeed);
            }
        }
        indexDialogue++;
        dialogue.text = text;
        skipped = true;
        yield return null;   
    }

    IEnumerator IEndCutScene (Collider2D collider)
    {
        CameraBehavior behavior = mainCamera.GetComponent<CameraBehavior>();
        behavior.fadeDirection = 1;
        yield return new WaitForSeconds(.8f);
        warningSign.SetActive(false);
        behavior.fadeDirection = -1;
        dialogue.text = "";
        indexDialogue = 0;
        if (collider != null)
        {
            if( collider.name == "Satelite" )
            {
                cabos.gameObject.SetActive(true);
            }
            if (collider.name == "Foguete")
            {
                metais.gameObject.SetActive(true);
            }
            if(collider.name == "Celular-gigante")
            {
                baterias.gameObject.SetActive(true);
            }
            Destroy(collider.gameObject);
        }
        currentDialogue = ";".Split(';') ;
        canMove = true;
        yield return new WaitForSeconds(.8f);
        yield return null;
    }

    IEnumerator IEndScene ()
    {
        mainCamera.GetComponent<CameraBehavior>().fadeDirection = 1;
        yield return new WaitForSeconds(1f);
        string currentSceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentSceneName);
        yield return null;
    }
}