﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playButton : MonoBehaviour 
{
    public Camera mainCamera;
    public AudioSource playSound;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    public void GoToLevel(string level)
    {
        CameraBehavior behavior = mainCamera.GetComponent<CameraBehavior>();
        playSound.Play();
        behavior.fadeDirection = 1;
        StartCoroutine(IStartScene(level));
    }

    IEnumerator IStartScene (string level)
    {
        yield return new WaitForSeconds(1.4f);
        SceneManager.LoadScene(level);
    }
}