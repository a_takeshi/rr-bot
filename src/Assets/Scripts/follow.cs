using UnityEngine;
using System.Collections;

public class follow : MonoBehaviour {

	public GameObject player;
	public float offset;
    public float offsetX;
    public GameObject imageBound;
    private float hBounds;
    private float _boundLeft;
    private float _boundRight;

    void Awake()
    {
        imageBound = GameObject.Find("ground main");
        hBounds = Camera.main.orthographicSize * Screen.width / Screen.height;
        _boundLeft = hBounds - imageBound.GetComponent<Renderer>().bounds.size.x / 2 ;
        _boundRight = imageBound.GetComponent<Collider2D>().bounds.size.x /2 - hBounds;
    }
    void LateUpdate () 
	{
        var pos = player.transform.position;
        pos.x = Mathf.Clamp(player.transform.position.x, _boundLeft + offsetX, _boundRight + offsetX);
		transform.position = new Vector2( pos.x, offset);
	}
}