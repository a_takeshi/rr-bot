﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CameraBehavior : MonoBehaviour {

    public float alphaFadeValue;
    public float fadeSpeed;
    public int fadeDirection;
    public bool isFinishedFading;
    public Texture2D blackTexture;

    void Start() {
        alphaFadeValue = 1;
    }

    void OnGUI()
    {
        GUI.color = new Color(0, 0, 0, alphaFadeValue);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blackTexture);
        alphaFadeValue += fadeSpeed * fadeDirection * Time.deltaTime;
        
        if (alphaFadeValue <= 0 )
        {
            alphaFadeValue = 0;
            isFinishedFading = true;
        }
        else if (alphaFadeValue >= 1)
        {
            alphaFadeValue = 1;
            isFinishedFading = true;
        }
        else
        {
            isFinishedFading = false;
        }
    }

    void FixedUpdate () {
        
	}
}
