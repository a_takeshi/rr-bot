﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class AdvanceScene : MonoBehaviour {

    [SerializeField]
    private string levelName;
    private CameraBehavior mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main.GetComponent<CameraBehavior>();
    }

    void Start()
    {
        mainCamera.fadeDirection = -1;
    }

    void Update () {
		if(Input.GetKeyDown(KeyCode.Z))
        {
            SceneManager.LoadScene(levelName);
            mainCamera.fadeDirection = 1;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
